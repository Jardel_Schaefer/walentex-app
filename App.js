import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, StatusBar, Image, TextInput } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="red" hidden={false} translucent={false} /> 
      <Image source={require('./public/walentex.png')} style={styles.logo} />
      <Text style={{fontSize: 24, marginTop: 20, color:'#daa520', fontWeight:'bold'}} >Área do </Text>
      <Text style={{fontSize: 24,color:'#daa520', fontWeight:'bold'}} >Representante </Text>
        <View style={styles.container2} >
        <TextInput style={styles.inputLogin} placeholder="Usuário"></TextInput>
        <TextInput style={styles.inputLogin} placeholder="Senha"></TextInput>
        </View>
     
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    // borderTopColor:'#daa520',
    // borderTopWidth: 25,
  

    
  },
    container2: {
    flex: 1,
    backgroundColor: '#fff',
    width:'100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop:40
  },
  logo:{
    width:100,
    height: 100,
    marginTop:60

  },

  formLogin:{
    marginTop:20
  },

  inputLogin:{
    borderBottomColor:'#A9A9A9',
    borderBottomWidth: 2,
    height:40,
    width:'85%',
    marginTop:10,
    fontSize: 20
    
  }
});
